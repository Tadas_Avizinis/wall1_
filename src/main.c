/**
  ******************************************************************************
  * @file    Examples_LL/USART/USART_Communication_Tx/Src/main.c
  * @author  MCD Application Team
  * @brief   This example describes how to send bytes over USART IP using
  *          the STM32L4xx USART LL API.
  *          Peripheral initialization done using LL unitary services functions.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "tinyexpr.h"
#include "tinyexpr.c"
#include <string.h>

#define RX_BUFFER_SIZE   128

uint8_t *input;
__IO uint32_t i = 0;
__IO uint8_t ubButtonPress = 0;
uint8_t ubSend = 0;

uint8_t aRXBufferA[RX_BUFFER_SIZE];
uint8_t aRXBufferB[RX_BUFFER_SIZE];
__IO uint32_t     uwNbReceivedChars;
__IO uint32_t     uwBufferReadyIndication;
uint8_t *pBufferReadyForUser;
uint8_t *pBufferReadyForReception;


/* Private function prototypes -----------------------------------------------*/
void     SystemClock_Config(void);
void     Configure_USART(void);
void     StartReception(void);
void     Calculate(char *input);
void     Trim_Str(char *expression, int length, int action);
void     Make_Derivative(char *expresssion, int length);
void     Trim_White_Space(char *str);
void     Replace_x(char *expression);
void     PrintInfo(uint8_t *String, uint32_t Size);
void     WaitForUserButtonPress(void);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
  /* Configure the system clock to 80 MHz */
  SystemClock_Config();

  /* Configure USARTx (USART IP configuration and related GPIO initialization) */
  Configure_USART();

  StartReception();

  /* Infinite loop */
  while (1)
  {
  }
}

void Configure_USART(void)
{

  /* (1) Enable GPIO clock and configures the USART pins *********************/

  /* Enable the peripheral clock of GPIO Port */
  USARTx_GPIO_CLK_ENABLE();

  /* Configure Tx Pin as : Alternate function, High Speed, Push pull, Pull up */
  //LL_GPIO_SetPinMode(USARTx_TX_GPIO_PORT, USARTx_TX_PIN, LL_GPIO_MODE_ALTERNATE);
  LL_GPIO_SetPinMode(USARTx_TX_GPIO_PORT, USARTx_TX_PIN, LL_GPIO_MODE_ALTERNATE);
  USARTx_SET_TX_GPIO_AF();
  //LL_GPIO_SetAFPin_0_7(USARTx_TX_GPIO_PORT, USARTx_TX_PIN, LL_GPIO_AF_7);
  LL_GPIO_SetPinSpeed(USARTx_TX_GPIO_PORT, USARTx_TX_PIN, LL_GPIO_SPEED_FREQ_HIGH);
  LL_GPIO_SetPinOutputType(USARTx_TX_GPIO_PORT, USARTx_TX_PIN, LL_GPIO_OUTPUT_PUSHPULL);
  LL_GPIO_SetPinPull(USARTx_TX_GPIO_PORT, USARTx_TX_PIN, LL_GPIO_PULL_UP);

  /* Configure Rx Pin as : Alternate function, High Speed, Push pull, Pull up */
  LL_GPIO_SetPinMode(USARTx_RX_GPIO_PORT, USARTx_RX_PIN, LL_GPIO_MODE_ALTERNATE);
  //LL_GPIO_SetPinMode(USARTx_TX_GPIO_PORT, GPIO_BSRR_BS1, LL_GPIO_MODE_ALTERNATE);
  USARTx_SET_RX_GPIO_AF();
  //LL_GPIO_SetAFPin_0_7(USARTx_RX_GPIO_PORT, USARTx_RX_PIN, LL_GPIO_AF_7);
  LL_GPIO_SetPinSpeed(USARTx_RX_GPIO_PORT, USARTx_RX_PIN, LL_GPIO_SPEED_FREQ_HIGH);
  LL_GPIO_SetPinOutputType(USARTx_RX_GPIO_PORT, USARTx_RX_PIN, LL_GPIO_OUTPUT_PUSHPULL);
  LL_GPIO_SetPinPull(USARTx_RX_GPIO_PORT, USARTx_RX_PIN, LL_GPIO_PULL_UP);


/* (2) NVIC Configuration for USART interrupts */
  /*  - Set priority for USARTx_IRQn */
  /*  - Enable USARTx_IRQn */
  NVIC_SetPriority(USARTx_IRQn, 0);  
  NVIC_EnableIRQ(USARTx_IRQn);



  /* (2) Enable USART peripheral clock and clock source ***********************/
  USARTx_CLK_ENABLE();

  /* Set clock source */
  USARTx_CLK_SOURCE();

  /* TX/RX direction */
  LL_USART_SetTransferDirection(USARTx_INSTANCE, LL_USART_DIRECTION_TX_RX);

  /* 8 data bit, 1 start bit, 1 stop bit, no parity */
  LL_USART_ConfigCharacter(USARTx_INSTANCE, LL_USART_DATAWIDTH_8B, LL_USART_PARITY_NONE, LL_USART_STOPBITS_1);

  LL_USART_SetBaudRate(USARTx_INSTANCE, SystemCoreClock, LL_USART_OVERSAMPLING_16, 9600); 

  /* (4) Enable USART *********************************************************/
  LL_USART_Enable(USARTx_INSTANCE);

  /* Polling USART initialisation */
  while((!(LL_USART_IsActiveFlag_TEACK(USARTx_INSTANCE))) || (!(LL_USART_IsActiveFlag_REACK(USARTx_INSTANCE))))
  { 
  }

  LL_USART_EnableIT_RXNE(USARTx_INSTANCE);
  LL_USART_EnableIT_ERROR(USARTx_INSTANCE);
}

void StartReception(void)
{
  /* Initializes Buffer swap mechanism :
     - 2 physical buffers aRXBufferA and aRXBufferB (RX_BUFFER_SIZE length)
     
  */
  pBufferReadyForReception = aRXBufferA;
  pBufferReadyForUser      = aRXBufferB;
  uwNbReceivedChars = 0;
  uwBufferReadyIndication = 0;

  /* Print user info on PC com port */
  PrintInfo("reset", sizeof("reset"));

  /* Clear Overrun flag, in case characters have already been sent to USART */
  LL_USART_ClearFlag_ORE(USARTx_INSTANCE);

}


void PrintInfo(uint8_t *String, uint32_t Size)
{
  uint32_t index = 0;
  uint8_t *pchar = String;
  
  /* Send characters one per one, until last char to be sent */
  for (index = 0; index < Size; index++)
  {
    /* Wait for TXE flag to be raised */
    while (!LL_USART_IsActiveFlag_TXE(USARTx_INSTANCE))
    {
    }

    /* Write character in Transmit Data register.
       TXE flag is cleared by writing data in TDR register */
    LL_USART_TransmitData8(USARTx_INSTANCE, *pchar++);
  }

  /* Wait for TC flag to be raised for last char */
  while (!LL_USART_IsActiveFlag_TC(USARTx_INSTANCE))
  {
  }
}

void SystemClock_Config(void)
{
  /* MSI configuration and activation */
  LL_FLASH_SetLatency(LL_FLASH_LATENCY_4);
  LL_RCC_MSI_Enable();
  while(LL_RCC_MSI_IsReady() != 1) 
  {
  };
  
  /* Main PLL configuration and activation */
  LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_MSI, LL_RCC_PLLM_DIV_1, 40, LL_RCC_PLLR_DIV_2);
  LL_RCC_PLL_Enable();
  LL_RCC_PLL_EnableDomain_SYS();
  while(LL_RCC_PLL_IsReady() != 1) 
  {
  };
  
  /* Sysclk activation on the main PLL */
  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
  while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL) 
  {
  };
  
  /* Set APB1 & APB2 prescaler*/
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
  LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);

  LL_Init1msTick(80000000);
  
  /* Update CMSIS variable (which can be updated also through SystemCoreClockUpdate function) */
  LL_SetSystemCoreClock(80000000);
}


void USART_CharReception_Callback(void)
{
  uint8_t *ptemp;int state = 0;
  /* Read Received character. RXNE flag is cleared by reading of RDR register */
  /*received_char = LL_USART_ReceiveData8(USARTx_INSTANCE);*/
  
  pBufferReadyForReception[uwNbReceivedChars++] = LL_USART_ReceiveData8(USARTx_INSTANCE);


  if(aRXBufferA[uwNbReceivedChars-1] == 13){

    uwBufferReadyIndication = 1;
    state = 1;

    Calculate(aRXBufferA);
    *pBufferReadyForReception = NULL;
    memset(aRXBufferA,0,RX_BUFFER_SIZE); 

  }else {
    if(aRXBufferB[uwNbReceivedChars-1] == 13){
      
      uwBufferReadyIndication = 1;
      state = 1;

      Calculate(aRXBufferB);
      *pBufferReadyForReception = NULL;
      memset(aRXBufferB,0,RX_BUFFER_SIZE); 
      }
  }

  if(state == 1){
    ptemp = pBufferReadyForUser;
    pBufferReadyForUser = pBufferReadyForReception;
    pBufferReadyForReception = ptemp;
    uwNbReceivedChars = 0;
  }  
}


void Calculate(char *input){

	double x = 1;
	te_variable vars[] = { { "x", &x }};
	int err;

  Trim_White_Space(input);

  char *expression_derivative = (char*) malloc (strlen (input)+1);
  strcpy(expression_derivative, input);

  char *expression_original = (char*) malloc (strlen (expression_derivative)+1);
  strcpy(expression_original, expression_derivative);

  Trim_Str(expression_original, sizeof(expression_original), 1);

  Make_Derivative(expression_derivative, sizeof(expr));


	te_expr *expr1 = te_compile(expression_original, vars, 1, &err);
	te_expr *expr2 = te_compile(expression_derivative, vars, 1, &err);
 
	for (int i = 0; i < 20; i++) {
 
		const double result1 = te_eval(expr1);
		const double result2 = te_eval(expr2);
    
    x = x - (result1 / result2);
	}

  int x_int = x + 0.5 - (x<0);

  if(x_int > 9){
    char text[2];
    text[0] = (x_int / 10) + 48;
    text[1] = (x_int % 10) + 48;
    
    PrintInfo(text, sizeof(text));
  }
  else{
    char text[1];
    text[0] = x_int + 48;
    
    PrintInfo(text, sizeof(text));
  }

}

void Make_Derivative(char *expression, int length){

int pos;
char *position;

if (strchr(expression, '^') != NULL) {
    char *position = strchr(expression, '^');
    pos = position - expression;
    expression[pos] = '*';
    expression[pos-1] = 'y';
    Replace_x(expression);
    expression[pos-1] = 'x';

    Trim_Str(expression, length, 0);

}else{
    Replace_x(expression);
    Trim_Str(expression, length, 0);
  }
}


void Replace_x(char *expression){
  int pos;
  char *position;

  position = strchr(expression, 'x');
  pos = position - expression;
  expression[pos] = '1';
  
  if(strchr(expression, 'x') != NULL){
    position = strchr(expression, 'x');
    pos = position - expression;

    expression[pos] = '1';
  }
}

void Trim_Str(char *expression, int length, int action){
  int pos = strlen(expression); int temp;
  if(action == 0){
  for(int i = 1; i < 4; i++){
    temp = pos-i;
    expression[temp] = 0;
  }

  while(isdigit(expression[pos-4])){
      expression[pos-4] = 0;
      pos--;
  }
  expression[pos-4] = 0;
  }else{
    for(int i = 1; i < 3; i++){
      temp = pos-i;
      expression[temp] = 0;
    }
    if(expression[pos-3] == '='){
      expression[pos-3] = 0;
    }
  }
}


void Trim_White_Space(char *s)
{
    char * p = s;
    int l = strlen(p);

    while(isspace(p[l - 1])) p[--l] = 0;
    while(* p && isspace(* p)) ++p, --l;

    memmove(s, p, l + 1);
}


void USARTx_IRQHandler(void)
{
  /* Check RXNE flag value in ISR register */
  if(LL_USART_IsActiveFlag_RXNE(USARTx_INSTANCE) && LL_USART_IsEnabledIT_RXNE(USARTx_INSTANCE))
  {
    /* RXNE flag will be cleared by reading of RDR register (done in call) */
    /* Call function in charge of handling Character reception */
    USART_CharReception_Callback();
  }
  else
  {
    /* Call Error function */
  }
	
}

